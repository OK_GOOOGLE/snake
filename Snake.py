import curses # documentation:   https://docs.python.org/3/library/curses.html#curses.newwin
              # or here :        https://docs.python.org/2/library/curses.html
from curses import KEY_UP, KEY_LEFT, KEY_RIGHT, KEY_DOWN
from random import randint


WIDTH, HEIGHT = 55, 20
MAX_X, MAX_Y = WIDTH - 2, HEIGHT - 2
SNAKE_LENGTH = 5
SNAKE_X = SNAKE_LENGTH + 1 # position of the head
SNAKE_Y = 3
TIMEOUT = 100 # speed

class Snake(object):
    REV_DIR_MAP = { # structure for handling the case in which player tries to change moving direction by 180 degrees
        KEY_UP: KEY_DOWN,
        KEY_DOWN: KEY_UP,
        KEY_LEFT: KEY_RIGHT,
        KEY_RIGHT:KEY_LEFT
    }
    def __init__(self, x, y, window):
        self.body_list = []
        self.hit_score = 0 # at the beginning
        self.timeout = TIMEOUT

        for i in range(SNAKE_LENGTH, 0, -1):
            self.body_list.append(Body(x - i, y))

        self.body_list.append(Body(x, y, '0'))

        self.window = window

        self.direction = KEY_RIGHT

        self.last_head_coor = (x, y)

        self.direction_map = {
            KEY_UP:    self.move_up,
            KEY_DOWN:  self.move_down,
            KEY_LEFT:  self.move_left,
            KEY_RIGHT: self.move_right
        }

    @property
    def score(self):
        return "Score: {}".format(self.hit_score)

    def add_body(self, body_list):
        self.body_list.extend(body_list) # extends list by appending elements from an iterable argument

    def eat_food(self, food):
        food.reset()

        body = Body(self.last_head_coor[0], self.last_head_coor[1])
        self.body_list.insert(-1, body)
        self.hit_score += 1

        if self.hit_score % 3 == 0:
            self.timeout -= 5
            self.window.timeout(self.timeout)

    @property
    def collided(self):
        return any([body.coor == self.head.coor \
                    for body in self.body_list[:-1]]) # returns true if any of iterable's elements is true

    def update(self):
        last_body = self.body_list.pop(0)

        last_body.x = self.body_list[-1].x
        last_body.y = self.body_list[-1].y
        self.body_list.insert(-1, last_body)
        self.last_head_coor = (self.head.x, self.head.y)
        self.direction_map[self.direction]()

    def change_direction(self, direction):
        if direction != Snake.REV_DIR_MAP[self.direction]: # avoids reversing direction
            self.direction = direction

    def render(self):
        for body in self.body_list:
            self.window.addstr(body.y, body.x, body.segment)

    @property
    def head(self):
        return self.body_list[-1]

    @property
    def coor(self):
        return self.head.x, self.head.y

    #moving functions
    def move_up(self):
        self.head.y -= 1
        if self.head.y < 1:
            self.head.y = MAX_Y

    def move_down(self):
        self.head.y += 1
        if self.head.y > MAX_Y:
            self.head.y = 1

    def move_left(self):
        self.head.x -= 1
        if self.head.x < 1:
            self.head.x = MAX_X

    def move_right(self):
        self.head.x += 1
        if self.head.x > MAX_X:
            self.head.x = 1



class Body(object):
    def __init__(self, x, y,  segment = '='):
        self.x, self.y = x, y
        self.segment = segment

    @property
    def coor(self):
        return self.x, self.y



class Food(object):
    def __init__(self, window, char = '*'):
        self.x = randint(1, MAX_X)
        self.y = randint(1, MAX_Y)

        self.char   = char
        self.window = window

    def render(self):
        self.window.addstr(self.y, self.x, self.char)

    def reset(self):
        self.x = randint(1, MAX_X)
        self.y = randint(1, MAX_Y)

if __name__ == '__main__': # if this is the main file(executed directly by us)
    won = False
    curses.initscr()
    curses.beep()
    curses.beep()

    window = curses.newwin(HEIGHT, WIDTH, 0, 0)
    window.timeout(TIMEOUT)
    window.keypad(1) # enabling keypad mode which allows curses to process all key events returning special key values (like curses.KEY_LEFT)

    curses.noecho() # turn off displaying pressed keys
    curses.curs_set(0) # removes blinking cursor
    window.border(0) # border around the window. 0 for any parameter causes the default character to be used for that parameter.

    #creating a snake and some food
    snake = Snake(SNAKE_X, SNAKE_Y, window)
    food  = Food(window, '*')
    food1 = Food(window, '*')

    while True:
        window.clear()
        window.border(0)
        window.addstr(29, 1, "Esc - exit") # y, x
        snake.render()
        food1.render()
        food.render()
        window.addstr(0, 4, snake.score) # displaying score
        if snake.hit_score >= 5:
            window.addstr(0, 15, "Congrats")
        if snake.hit_score >= 10:
            window.addstr(0, 15, "WOW, Still playing??")
        if snake.hit_score >= 15:
            window.addstr(0, 15, "Can't believe it... Stop wasting time!")
        event = None
        event = window.getch() # useeer input. returns int values


        if event == 27: # escape
            break

        #changig direction
        if event in [KEY_UP, KEY_DOWN, KEY_LEFT, KEY_RIGHT]:
            snake.change_direction(event)
        #eating food
        if snake.head.x == food.x and snake.head.y == food.y:
            snake.eat_food(food)
        if snake.head.x == food1.x and snake.head.y == food1.y:
            snake.eat_food(food1)

        # if event == 32:
        #     key = -1
        #     while key != 32:
        #         key = window.getch()

        snake.update()

        if snake.collided:
            break

        if len(snake.body_list) == 30:
            won = True
            break

    curses.echo()
    curses.endwin()
    if (won == True):
        print ("Congratulations! You've just wasted your time playing this meaningless game! But at least you won it.")


